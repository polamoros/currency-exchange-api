<?php

namespace PolAmoros\CurrencyExchangeApi\Domain\Exchange;

use JsonSerializable;

class Exchange implements JsonSerializable
{
    /**
     * @var int
     */
    private $base;

    /**
     * @var string
     */
    private $quote;

    /**
     * @var string
     */
    private $rate;


    /**
     * @param string $base
     * @param string $quote
     * @param float $rate
     */
    public function __construct(string $base, string $quote, float $rate)
    {
        $this->base = $base;
        $this->quote = $quote;
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getBase(): string
    {
        return $this->base;
    }

    /**
     * @return string
     */
    public function getQuote(): string
    {
        return $this->quote;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'base' => $this->base,
            'quote' => $this->quote,
            'rate' => $this->rate
        ];
    }
}
