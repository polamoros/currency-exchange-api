<?php


namespace PolAmoros\CurrencyExchangeApi\Application\Actions\Exchange;

use Psr\Http\Message\ResponseInterface as Response;

class ListExchangesAction extends AbstractExchangeAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $baseCurrency = $this->resolveArg('base');
        $exchanges = $this->exchangeRepository->findExchangesByBase($baseCurrency);
        return $this->respondWithData($exchanges);
    }
}
