<?php

namespace PolAmoros\CurrencyExchangeApi\Domain\Exchange;

interface ExchangeRepositoryInterface
{
    const EXCHANGE_NOT_FOUND_EXCEPTION = 'The exchange you requested does not exist.';

    /**
     * @param string $base
     * @return Exchange[]
     */
    public function findExchangesByBase(string $base): array;

    /**
     * @param string $base
     * @param string $quote
     * @return Exchange
     * @throws ExchangeNotFoundException
     */
    public function findExchangeByBaseAndQuote(string $base, string $quote): Exchange;
}
