<?php
declare(strict_types=1);

use PolAmoros\CurrencyExchangeApi\Domain\Exchange\ExchangeRepositoryInterface;
use PolAmoros\CurrencyExchangeApi\Infrastructure\Persistence\Exchange\InMemoryExchangeRepository;
use DI\Container;
use Slim\App;
use function DI\autowire;

return function (App $app) {
    /** @var Container $container */
    $container = $app->getContainer();

    // Here we map our ExchangeRepository interface to its in memory implementation
    $container->set(ExchangeRepositoryInterface::class, autowire(InMemoryExchangeRepository::class));
};
