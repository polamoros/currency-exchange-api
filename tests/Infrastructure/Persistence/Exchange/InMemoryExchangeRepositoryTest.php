<?php

namespace PolAmoros\CurrencyExchangeApi\Tests\Infrastructure\Persistence\Exchange;

use PolAmoros\CurrencyExchangeApi\Domain\Exchange\Exchange;
use PolAmoros\CurrencyExchangeApi\Domain\Exchange\ExchangeNotFoundException;
use PolAmoros\CurrencyExchangeApi\Domain\Exchange\ExchangeRepositoryInterface;
use PolAmoros\CurrencyExchangeApi\Infrastructure\Persistence\Exchange\InMemoryExchangeRepository;
use PolAmoros\CurrencyExchangeApi\Tests\TestCase;

class InMemoryExchangeRepositoryTest extends TestCase
{
    public function testFindExchangeByBase(): void
    {
        $exchange = new Exchange('EUR', 'USD', 1.13);
        $currencyRepository = new InMemoryExchangeRepository([$exchange]);
        $this->assertEquals([$exchange], $currencyRepository->findExchangesByBase('EUR'));
    }

    /**
     * @throws ExchangeNotFoundException
     */
    public function testFindExchangeByBaseAndQuote(): void
    {
        $exchange = new Exchange('EUR', 'USD', 1.13);

        $currencyRepository = new InMemoryExchangeRepository([$exchange]);

        $this->assertEquals($exchange, $currencyRepository->findExchangeByBaseAndQuote('EUR', 'USD'));
    }

    /**
     * @throws ExchangeNotFoundException
     */
    public function testFindExchangeByBaseAndQuoteThrowsNotFoundException(): void
    {
        $currencyRepository = new InMemoryExchangeRepository([]);
        $this->expectException(ExchangeNotFoundException::class);
        $this->expectExceptionMessage(ExchangeRepositoryInterface::EXCHANGE_NOT_FOUND_EXCEPTION);
        $currencyRepository->findExchangeByBaseAndQuote('EUR', 'USD');
    }
}
