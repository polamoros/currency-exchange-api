<?php

namespace PolAmoros\CurrencyExchangeApi\Tests\Application\Actions\Exchange;

use Exception;
use PolAmoros\CurrencyExchangeApi\Application\Actions\ActionError;
use PolAmoros\CurrencyExchangeApi\Application\Actions\ActionPayload;
use PolAmoros\CurrencyExchangeApi\Application\Handlers\HttpErrorHandler;
use PolAmoros\CurrencyExchangeApi\Domain\Exchange\Exchange;
use PolAmoros\CurrencyExchangeApi\Domain\Exchange\ExchangeNotFoundException;
use PolAmoros\CurrencyExchangeApi\Domain\Exchange\ExchangeRepositoryInterface;
use DI\Container;
use PolAmoros\CurrencyExchangeApi\Infrastructure\Persistence\Exchange\InMemoryExchangeRepository;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;
use PolAmoros\CurrencyExchangeApi\Tests\TestCase;

class ViewExchangeActionTest extends TestCase
{
    const BASE_CURRENCY_TEST = 'EUR';
    const QUOTE_CURRENCY_TEST = 'USD';
    const EXCHANGE_RATE_TEST = 1.13;

    /**
     * @throws Exception
     */
    public function testAction(): void
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $exchange = new Exchange(self::BASE_CURRENCY_TEST, self::QUOTE_CURRENCY_TEST, self::EXCHANGE_RATE_TEST);

        $exchangeRepositoryMock = $this->getMockBuilder(InMemoryExchangeRepository::class)
            ->setMethods(['findExchangeByBaseAndQuote'])
            ->getMock();
        $exchangeRepositoryMock
            ->expects($this->once())
            ->method('findExchangeByBaseAndQuote')
            ->willReturn($exchange);

        $container->set(ExchangeRepositoryInterface::class, $exchangeRepositoryMock);

        $request = $this->createRequest(
            'GET',
            '/v1/exchanges/bases/' . self::BASE_CURRENCY_TEST . '/quotes/' . self::QUOTE_CURRENCY_TEST
        );
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedPayload = new ActionPayload(200, $exchange);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    /**
     * @throws Exception
     */
    public function testActionThrowsExchangeNotFoundException(): void
    {
        $app = $this->getAppInstance();
        // Setup Error Handler
        $app->add($this->getErrorHandler($app));

        /** @var Container $container */
        $container = $app->getContainer();

        $exchangeRepositoryMock = $this->getMockBuilder(InMemoryExchangeRepository::class)
            ->setMethods(['findExchangeByBaseAndQuote'])
            ->getMock();
        $exchangeRepositoryMock
            ->expects($this->once())
            ->method('findExchangeByBaseAndQuote')
            ->willThrowException(new ExchangeNotFoundException(
                ExchangeRepositoryInterface::EXCHANGE_NOT_FOUND_EXCEPTION
            ));

        $container->set(ExchangeRepositoryInterface::class, $exchangeRepositoryMock);
        $request = $this->createRequest(
            'GET',
            '/v1/exchanges/bases/' . self::BASE_CURRENCY_TEST . '/quotes/' . self::QUOTE_CURRENCY_TEST
        );
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedError = new ActionError(
            ActionError::RESOURCE_NOT_FOUND,
            ExchangeRepositoryInterface::EXCHANGE_NOT_FOUND_EXCEPTION
        );
        $expectedPayload = new ActionPayload(404, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    /**
     * @param $app
     * @return ErrorMiddleware
     */
    protected function  getErrorHandler(App $app): ErrorMiddleware
    {
        $callableResolver = $app->getCallableResolver();
        $responseFactory = $app->getResponseFactory();
        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware(
            $callableResolver,
            $responseFactory,
            true,
            false ,
            false
        );
        $errorMiddleware->setDefaultErrorHandler($errorHandler);
        return $errorMiddleware;
    }
}
