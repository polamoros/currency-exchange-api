<?php

namespace PolAmoros\CurrencyExchangeApi\Tests\Domain\Exchange;

use PolAmoros\CurrencyExchangeApi\Domain\Exchange\Exchange;
use PolAmoros\CurrencyExchangeApi\Tests\TestCase;

class ExchangeTest extends TestCase
{
    public function exchangeProvider(): array
    {
        return [
            ['EUR', 'USD', 1.13],
            ['EUR', 'CAD', 1.47],
        ];
    }

    /**
     * @dataProvider exchangeProvider
     * @param $base
     * @param $quote
     * @param $rate
     */
    public function testGetters($base, $quote, $rate): void
    {
        $exchange = new Exchange($base, $quote, $rate);
        $this->assertEquals($base, $exchange->getBase());
        $this->assertEquals($quote, $exchange->getQuote());
        $this->assertEquals($rate, $exchange->getRate());
    }

    /**
     * @dataProvider exchangeProvider
     * @param $base
     * @param $quote
     * @param $rate
     */
    public function testJsonSerialize($base, $quote, $rate): void
    {
        $exchange = new Exchange($base, $quote, $rate);
        $expectedPayload = json_encode([
            'base' => $base,
            'quote' => $quote,
            'rate' => $rate,
        ]);
        $this->assertEquals($expectedPayload, json_encode($exchange));
    }
}
