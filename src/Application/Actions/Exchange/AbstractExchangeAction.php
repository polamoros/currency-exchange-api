<?php


namespace PolAmoros\CurrencyExchangeApi\Application\Actions\Exchange;

use PolAmoros\CurrencyExchangeApi\Application\Actions\AbstractAction;
use PolAmoros\CurrencyExchangeApi\Domain\Exchange\ExchangeRepositoryInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractExchangeAction extends AbstractAction
{
    /**
     * @var ExchangeRepositoryInterface
     */
    protected $exchangeRepository;

    /**
     * @param LoggerInterface $logger
     * @param ExchangeRepositoryInterface  $exchangeRepository
     */
    public function __construct(LoggerInterface $logger, ExchangeRepositoryInterface $exchangeRepository)
    {
        parent::__construct($logger);
        $this->exchangeRepository = $exchangeRepository;
    }
}
