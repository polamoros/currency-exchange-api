<?php

namespace PolAmoros\CurrencyExchangeApi\Infrastructure\Persistence\Exchange;

use PolAmoros\CurrencyExchangeApi\Domain\Exchange\Exchange;
use PolAmoros\CurrencyExchangeApi\Domain\Exchange\ExchangeNotFoundException;
use PolAmoros\CurrencyExchangeApi\Domain\Exchange\ExchangeRepositoryInterface;

class InMemoryExchangeRepository implements ExchangeRepositoryInterface
{
    /**
     * @var Exchange[]
     */
    private $exchanges;

    /**
     * InMemoryUserRepository constructor.
     *
     * @param array|null $exchanges
     */
    public function __construct(array $exchanges = null)
    {
        $this->exchanges = $exchanges ?? [
            new Exchange('EUR', 'USD', 1.13),
            new Exchange('EUR', 'CAD', 1.47),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        return $this->exchanges;
    }

    /**
     * {@inheritdoc}
     */
    public function findExchangesByBase($base): array
    {
        return array_filter(
            $this->exchanges,
            function (Exchange $exchange) use ($base) {
                return ($exchange->getBase() === $base);
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function findExchangeByBaseAndQuote(string $base, string $quote): Exchange
    {
        foreach ($this->exchanges as $exchange) {
            if ($exchange->getBase() === $base && $exchange->getQuote() === $quote) {
                return $exchange;
            }
        }
        throw new ExchangeNotFoundException(self::EXCHANGE_NOT_FOUND_EXCEPTION);
    }
}
