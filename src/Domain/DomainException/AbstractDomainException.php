<?php
declare(strict_types=1);

namespace PolAmoros\CurrencyExchangeApi\Domain\DomainException;

use Exception;

abstract class AbstractDomainException extends Exception
{
}
