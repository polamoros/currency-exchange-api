<?php

namespace PolAmoros\CurrencyExchangeApi\Domain\Exchange;

use PolAmoros\CurrencyExchangeApi\Domain\DomainException\DomainRecordNotFoundException;

class ExchangeNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The exchange you requested does not exist.';
}
