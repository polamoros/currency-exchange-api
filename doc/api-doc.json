{
  "swagger":"2.0",
  "info":{
    "title":"Currency exchange API",
    "description":"Documentation of the Currency Exchange API",
    "version":"1.0.0",
    "contact":{
      "email":"info@polamoros.com"
    }
  },
  "basePath":"/v1",
  "tags":[
    {
      "name":"exchange",
      "description":"Exchange"
    },
    {
      "name":"auth",
      "description":"Authentication required endpoints"
    }
  ],
  "paths":{
    "/exchanges/bases/{base}":{
      "get":{
        "tags":[
          "exchange"
        ],
        "description":"Return a list of exchange pairs given a base currency",
        "produces":[
          "application/json"
        ],
        "parameters":[
          {
            "name":"base",
            "in":"path",
            "description":"Base currency of the exchange (ISO 4217). Example: 'EUR'",
            "required":true,
            "type":"string"
          }
        ],
        "responses":{
          "200":{
            "description":"Successful request",
            "schema":{
              "type": "object",
              "properties": {
                "rates": {
                  "type" : "array",
                  "items" : {
                    "type" : "object",
                    "properties" : {
                      "base" : {
                        "type": "string",
                        "example": "EUR"
                      },
                      "quote" : {
                        "type": "string",
                        "example": "USD"
                      },
                      "rate" : {
                        "type": "number",
                        "example": "1.13"
                      }
                    }
                  }
                }
              }
            }
          },
          "404":{
            "description":"Currency not found",
            "schema":{
              "type": "object",
              "properties": {
                "code": {
                  "type" : "number",
                  "example" : "404"
                },
                "error": {
                  "type" : "string",
                  "example" : "The base currency was not found"
                }
              }
            }
          },
          "500":{
            "description":"Internal server error"
          }
        }
      }
    },
    "/exchanges/bases/{base}/quotes/{quote}":{
      "get":{
        "tags":[
          "exchange"
        ],
        "description":"Return the latest information of an exchange given the base and quote currencies",
        "produces":[
          "application/json"
        ],
        "parameters":[
          {
            "name":"base",
            "in":"path",
            "description":"Base currency of the exchange (ISO 4217). Example: 'EUR'",
            "required":true,
            "type":"string"
          },
          {
            "name":"quote",
            "in":"path",
            "description":"Quote currency of the exchange (ISO 4217). Example: 'USD'",
            "required":true,
            "type":"string"
          }
        ],
        "responses":{
          "200":{
            "description":"Successful request",
            "schema":{
              "type": "object",
              "properties": {
                "base" : {
                  "type": "string",
                  "example": "EUR"
                },
                "quote" : {
                  "type": "string",
                  "example": "USD"
                },
                "rate" : {
                  "type": "number",
                  "example": "1.13"
                }
              }
            }
          },
          "404":{
            "description":"Currency not found",
            "schema":{
              "type": "object",
              "properties": {
                "code": {
                  "type" : "number",
                  "example" : "404"
                },
                "error": {
                  "type" : "string",
                  "example" : "Base or quote currency was not found"
                }
              }
            }
          },
          "500":{
            "description":"Internal server error"
          }
        }
      },
      "put":{
        "tags":[
          "exchange",
          "auth"
        ],
        "description":"Update an existing exchange pair. Requires authentication.",
        "consumes":[
          "application/json"
        ],
        "produces":[
          "application/json"
        ],
        "parameters":[
          {
            "name":"base",
            "in":"path",
            "description":"Base currency of the exchange (ISO 4217). Example: 'EUR'",
            "required":true,
            "type":"string"
          },
          {
            "name":"quote",
            "in":"path",
            "description":"Destination currency of the exchange (ISO 4217). Example: 'USD'",
            "required":true,
            "type":"string"
          },
          {
            "in":"body",
            "name":"body",
            "description": "Rate for the exchange pair",
            "required":true,
            "schema":{
              "type" : "object",
              "properties" : {
                "rate": {
                  "type": "number",
                  "example": 1.14
                }
              }
            }
          }
        ],
        "responses":{
          "200":{
            "description":"successful request"
          },
          "401":{
            "description":"Unauthorized"
          },
          "404":{
            "description":"Currency not found",
            "schema":{
              "type": "object",
              "properties": {
                "code": {
                  "type" : "number",
                  "example" : "404"
                },
                "error": {
                  "type" : "string",
                  "example" : "Base or quote currency was not found"
                }
              }
            }
          },
          "405":{
            "description":"Invalid parameter",
            "schema":{
              "type": "object",
              "properties": {
                "code": {
                  "type" : "number",
                  "example" : "405"
                },
                "error": {
                  "type" : "string",
                  "example" : "'rate' parameter is invalid"
                }
              }
            }
          },
          "500":{
            "description":"Internal server error"
          }
        }
      }
    }
  }
}
