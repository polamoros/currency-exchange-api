Currency Exchange API
===========

API to request exchange rates

### Installation

1. Clone the repository
    ```sh
    git git@gitlab.com:polamoros/currency-exchange-api.git
    ```

2. Get into repository and execute
    `composer install`

### Usage

#### Run API
Execute `composer start` to run the API.

Execute `docker-compose up` to run the API using docker.

### Examples

* ##### Get rates from EUR base currency
    ```
    http://localhost:8080/v1/exchanges/bases/EUR
    ```
    Response 
    ```
    [
        {
            "base": "EUR",
            "quote": "USD",
            "rate": 1.13
        },
        {
            "base": "EUR",
            "quote": "CAD",
            "rate": 1.47
        }
    ]
    ```

* ##### Get rate from EUR base currency to USD
    ```
    http://localhost:8080/v1/exchanges/bases/EUR/quotes/USD
    ```
    Response
    ```
    {
        "base": "EUR",
        "quote": "USD",
        "rate": 1.13
    }
    ```
