<?php
declare(strict_types=1);

use PolAmoros\CurrencyExchangeApi\Application\Actions\Exchange\ListExchangesAction;
use PolAmoros\CurrencyExchangeApi\Application\Actions\Exchange\ViewExchangeAction;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->group('/{version}/exchanges/bases/{base}', function (Group $group) {
        $group->get('', ListExchangesAction::class);
        $group->get('/quotes/{quote}', ViewExchangeAction::class);
    });
};
