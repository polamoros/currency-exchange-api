<?php
declare(strict_types=1);

namespace PolAmoros\CurrencyExchangeApi\Domain\DomainException;

class DomainRecordNotFoundException extends AbstractDomainException
{
}
