<?php

namespace PolAmoros\CurrencyExchangeApi\Tests\Application\Actions\Exchange;

use PolAmoros\CurrencyExchangeApi\Application\Actions\ActionPayload;
use PolAmoros\CurrencyExchangeApi\Domain\Exchange\Exchange;
use PolAmoros\CurrencyExchangeApi\Domain\Exchange\ExchangeRepositoryInterface;
use PolAmoros\CurrencyExchangeApi\Infrastructure\Persistence\Exchange\InMemoryExchangeRepository;
use PolAmoros\CurrencyExchangeApi\Tests\TestCase;
use DI\Container;
use Exception;

class ListExchangeActionTest extends TestCase
{
    const BASE_CURRENCY_TEST = 'EUR';
    const QUOTE_CURRENCY_TEST = 'USD';
    const EXCHANGE_RATE_TEST = 1.13;

    /**
     * @throws Exception
     */
    public function testAction(): void
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $exchange = new Exchange(self::BASE_CURRENCY_TEST, self::QUOTE_CURRENCY_TEST, self::EXCHANGE_RATE_TEST);

        $exchangeRepositoryMock = $this->getMockBuilder(InMemoryExchangeRepository::class)
            ->setMethods(['findExchangesByBase'])
            ->getMock();
        $exchangeRepositoryMock
            ->expects($this->once())
            ->method('findExchangesByBase')
            ->willReturn([$exchange]);

        $container->set(ExchangeRepositoryInterface::class, $exchangeRepositoryMock);

        $request = $this->createRequest('GET', '/v1/exchanges/bases/' . self::BASE_CURRENCY_TEST);
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedPayload = new ActionPayload(200, [$exchange]);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
