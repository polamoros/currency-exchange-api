<?php


namespace PolAmoros\CurrencyExchangeApi\Application\Actions\Exchange;

use Psr\Http\Message\ResponseInterface as Response;

class ViewExchangeAction extends AbstractExchangeAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $baseCurrency = $this->resolveArg('base');
        $quoteCurrency = $this->resolveArg('quote');
        $exchange = $this->exchangeRepository->findExchangeByBaseAndQuote($baseCurrency, $quoteCurrency);
        return $this->respondWithData($exchange);
    }
}
